/*Sample Functions to Test */

function getCircleArea(radius) {
  return 3.1416 * radius ** 2;
}

function checkIfPassed(score, total) {
  return (score / total) * 100 >= 75;
}

function getAverage(num1, num2, num3, num4) {
  return (num1 + num2 + num3 + num4) / 4;
}

function getSum(num1, num2) {
  return num1 + num2;
}

function getDifference(num1, num2) {
  return num1 - num2;
}

const factorial = (n) => {
  if (typeof n !== "number") return undefined;
  if (n < 0) return undefined;
  if (n === 0) return 1;
  if (n === 1) return 1;

  return n * factorial(n - 1);
};

//ACTIVITY

function div_check(num1, num2, num3) {
  if (num1 % num2 == 0) {
    return true;
  } else if (num1 % num3 == 0) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  getCircleArea: getCircleArea,
  checkIfPassed: checkIfPassed,
  getAverage: getAverage,
  getSum: getSum,
  getDifference: getDifference,
  factorial: factorial,
  div_check: div_check,
};
