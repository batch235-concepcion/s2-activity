const { expect, assert } = require("chai");
const {
  getCircleArea,
  checkIfPassed,
  getAverage,
  getSum,
  getDifference,
  factorial,
  div_check,
} = require("..//util.js");

//Test Suite example
describe("Test get area circle", () => {
  it("Test area of circle radius 15 is 706.86", () => {
    let area = getCircleArea(15);
    assert.equal(area, 706.86);
  });
  it("Test area of circle radius 9 is 254.46959999999999", () => {
    let area = getCircleArea(9);
    expect(area).to.equal(254.46959999999999);
  });
});

describe("Test check if passed", () => {
  it("Test 25 out of 30 is passed", () => {
    let isPassed = checkIfPassed(25, 30);
    assert.equal(isPassed, true);
  });
  it("Test 30 out of 50 is not passed", () => {
    let isPassed = checkIfPassed(30, 50);
    assert.equal(isPassed, false);
  });
});

describe("Test average if passed", () => {
  it("assert that the average of 80,82,42 and 86 is 83", () => {
    let isPassed = getAverage(80, 82, 84, 86);
    expect(isPassed).to.equal(83);
  });
  it("assert that the average of 70,80,82 and 84 is 79", () => {
    let isPassed = getAverage(70, 80, 82, 84);
    expect(isPassed).to.equal(79);
  });
});

describe("Test get sum", () => {
  it("assert that the sum of 15 and 30 is 45", () => {
    let sum = getSum(15, 30);
    expect(sum).to.equal(45);
  });
  it("assert that the sum of 25 and 50 is 75", () => {
    let sum = getSum(25, 50);
    expect(sum).to.equal(75);
  });
});

describe("Test get difference", () => {
  it("assert that the difference of 70 and 40 is 30", () => {
    let sum = getDifference(70, 40);
    expect(sum).to.equal(30);
  });
  it("assert that the difference of 125 and 50 is 75", () => {
    let sum = getDifference(125, 50);
    expect(sum).to.equal(75);
  });
});

describe("Test factorials", () => {
  it("Test that 5! is 120", () => {
    const product = factorial(5);
    expect(product).to.equal(120);
  });
  it("Test that 1! is 1", () => {
    const product = factorial(1);
    expect(product).to.equal(1);
  });
  it("Test that 0! is 1", () => {
    const product = factorial(0);
    assert.equal(product, 1);
  });
  it("Test negative factorial is undefined", () => {
    const product = factorial(-1);
    assert.equal(product, undefined);
  });
  it("Test that non-numeric value returns an error", () => {
    const product = factorial("1");
    assert.equal(product, undefined);
  });
});

//ACTIVITY

describe("Test div_check", () => {
  it("Test that 120 should be divisible by 5", () => {
    const isDivisible = div_check(120, 5);
    assert.equal(isDivisible, true);
  });
  it("Test that 14 should be divisible by 7", () => {
    const isDivisible = div_check(14, 7);
    assert.equal(isDivisible, true);
  });
  it("Test that 105 should be divisible by 5 Or 7", () => {
    const isDivisible = div_check(105, 5, 7);
    assert.equal(isDivisible, true);
  });
  it("Test that 22 should not be divisible by 5 Or 7", () => {
    const isDivisible = div_check(105, 5, 7);
    assert.equal(isDivisible, true);
  });
});
